'use strict';

const btnChange = document.querySelector('.second-theme');
const footerTheme = document.querySelector('.footer');
const menuTheme = document.querySelector('.menu');

function setTheme() {
    footerTheme.classList.add (localStorage.getItem('theme'));
    menuTheme.classList.add (localStorage.getItem('theme'));
};

setTheme();

function changeTheme(){
    if (localStorage.getItem('theme') === 'default'){
        localStorage.setItem('theme', 'second');
        footerTheme.classList.remove('default');
        menuTheme.classList.remove('default');
    } else {
        localStorage.setItem('theme', 'default');
        footerTheme.classList.remove('second');
        menuTheme.classList.remove('second');
    }
    setTheme();
}


btnChange.addEventListener('click', ()=>{
    changeTheme();
})




